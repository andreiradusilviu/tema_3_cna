﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    enum RegexType
    {
        NONE,
        UNDERLINE,
        STAR,
        TILDA,
        APOSTROPHE
    }

    class MessageItem
    {
        String word;
        RegexType type;

        public MessageItem(String word, RegexType type)
        {
            this.word = word;
            this.type = type;
        }

        public string Word { get => word; set => word = value; }
        internal RegexType Type { get => type; set => type = value; }
    }
}
