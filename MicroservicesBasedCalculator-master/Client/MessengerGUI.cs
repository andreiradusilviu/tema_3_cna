﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using Grpc.Core;

namespace Client
{
    public partial class MessengerGUI : Form
    {
        const string Host = "localhost";
        const int Port = 16842;
        Generated.Broadcast.BroadcastClient client;
        Channel channel;
        List<MessageItem> messageItems;

        private AsyncDuplexStreamingCall<Generated.Message, Generated.Message> _chat;


        public MessengerGUI()
        {
            InitializeComponent();
            channel = new Channel($"{Host}:{Port}", ChannelCredentials.Insecure);
            client = new Generated.Broadcast.BroadcastClient(channel);
            messageItems = new List<MessageItem>();

            ColumnHeader header = new ColumnHeader();
            header.Text = "";
            header.Width = 342;
            listView.Columns.Add(header);
            listView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            listView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private async void ChatForm_Load(object sender, EventArgs e)
        {
            try
            {

                this._chat = client.CreateStream();

                _ = Task.Run(async () =>
                {
                    while (await _chat.ResponseStream.MoveNext(cancellationToken: CancellationToken.None))
                    {
                        var response = _chat.ResponseStream.Current;
                        Console.WriteLine($"{response.User.DisplayName}: {response.Message_}");
                        String message = ($"{response.User.DisplayName}: {response.Message_} {Environment.NewLine}"); //Environment.NewLine
                        messageItems = RegexChecker.GetFormatMessage(message);
                        AddItemsIntoListView();
                    }
                });
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        private void AddItemsIntoListView()
        {
            foreach (MessageItem item in messageItems)
            {
                ListViewItem listViewItem = new ListViewItem();
                switch (item.Type)
                {
                    case RegexType.NONE:
                        {
                            listViewItem.Text = item.Word;
                            listViewItem.UseItemStyleForSubItems = false;
                        }
                        break;
                    case RegexType.UNDERLINE:
                        {
                            listViewItem.Text = item.Word;
                            listViewItem.UseItemStyleForSubItems = false;
                            listViewItem.Font = new Font(listView.Font, FontStyle.Italic);
                        }
                        break;
                    case RegexType.STAR:
                        {
                            listViewItem.Text = item.Word;
                            listViewItem.UseItemStyleForSubItems = false;
                            listViewItem.Font = new Font(listView.Font, FontStyle.Bold);
                        }
                        break;
                    case RegexType.TILDA:
                        {
                            listViewItem.Text = item.Word;
                            listViewItem.UseItemStyleForSubItems = false;
                            listViewItem.Font = new Font(listView.Font, FontStyle.Strikeout);
                        }
                        break;
                    case RegexType.APOSTROPHE:
                        {
                            listViewItem.Text = item.Word;
                            listViewItem.UseItemStyleForSubItems = false;
                            listViewItem.Font = new Font(listView.Font, FontStyle.Underline);
                        }
                        break;
                }
                listView.Items.Add(listViewItem);
            }
            messageItems.Clear();
        }

        public async void sendBtn_Click(object sender, EventArgs e)
        {

            try
            {
                Generated.User user = new Generated.User();
                user.DisplayName = nameTextBox.Text;

                string line = messageTextBox.Text;
                if (_chat != null)
                {
                    await _chat.RequestStream.WriteAsync(new Generated.Message { User = user, Message_ = line });
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }

}
