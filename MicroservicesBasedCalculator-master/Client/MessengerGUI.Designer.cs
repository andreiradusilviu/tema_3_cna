﻿namespace Client
{
    partial class MessengerGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        
        private void InitializeComponent()
        {
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.messageTextBox = new System.Windows.Forms.TextBox();
            this.sendBtn = new System.Windows.Forms.Button();
            this.listView = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // nameTextBox
            // 
            this.nameTextBox.AccessibleName = "nameTextBox";
            this.nameTextBox.Location = new System.Drawing.Point(12, 403);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(134, 22);
            this.nameTextBox.TabIndex = 4;
            this.nameTextBox.Text = "Your name";
            // 
            // messageTextBox
            // 
            this.messageTextBox.AccessibleName = "messageTextBox";
            this.messageTextBox.Location = new System.Drawing.Point(174, 403);
            this.messageTextBox.Name = "messageTextBox";
            this.messageTextBox.Size = new System.Drawing.Size(378, 22);
            this.messageTextBox.TabIndex = 5;
            this.messageTextBox.Text = "Write an message";
            // 
            // sendBtn
            // 
            this.sendBtn.AccessibleName = "sendBtn";
            this.sendBtn.Location = new System.Drawing.Point(599, 393);
            this.sendBtn.Name = "sendBtn";
            this.sendBtn.Size = new System.Drawing.Size(159, 45);
            this.sendBtn.TabIndex = 6;
            this.sendBtn.Text = "SEND";
            this.sendBtn.UseVisualStyleBackColor = true;
            this.sendBtn.Click += new System.EventHandler(this.sendBtn_Click);
            // 
            // listView
            // 
            this.listView.FullRowSelect = true;
            this.listView.HideSelection = false;
            this.listView.LabelEdit = true;
            this.listView.Location = new System.Drawing.Point(8, 12);
            this.listView.Name = "listView";
            this.listView.Size = new System.Drawing.Size(784, 365);
            this.listView.TabIndex = 7;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.View = System.Windows.Forms.View.Details;
            // 
            // MessengerGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.listView);
            this.Controls.Add(this.sendBtn);
            this.Controls.Add(this.messageTextBox);
            this.Controls.Add(this.nameTextBox);
            this.Name = "MessengerGUI";
            this.Text = "MessengerGUI";
            this.Load += new System.EventHandler(this.ChatForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.TextBox nameTextBox;
        public System.Windows.Forms.TextBox messageTextBox;
        public System.Windows.Forms.Button sendBtn;
        private System.Windows.Forms.ListView listView;
    }
}