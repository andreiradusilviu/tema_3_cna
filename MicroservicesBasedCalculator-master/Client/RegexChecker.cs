﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Client
{
    class RegexChecker
    {
        private static string patternUnderline = @"_\s*\w+\s*\w+\s*_";
        private static string patternStar = @"\*\s*\w+\s*\w+\s*\*";
        private static string patternTilda = @"\~\s*\w+\s*\w+\s*\~";
        private static string patternApostrophe = @"\'\s*\w+\s*\w+\s*\'";


        public static String RegexCheckUnderlinePattern(String word)
        {
            String result = "";
            Regex regex = new Regex(patternUnderline);
            MatchCollection matcheditem = regex.Matches(word);
            if (matcheditem.Count > 0)
                result = matcheditem[0].Value;
            return result;
        }

        public static String RegexCheckStarPattern(String word)
        {
            String result = "";
            Regex regex = new Regex(patternStar);
            MatchCollection matcheditem = regex.Matches(word);
            if (matcheditem.Count > 0)
                result = matcheditem[0].Value;
            return result;
        }

        public static String RegexCheckTildaPattern(String word)
        {
            String result = "";
            Regex regex = new Regex(patternTilda);
            MatchCollection matcheditem = regex.Matches(word);
            if (matcheditem.Count > 0)
                result = matcheditem[0].Value;
            return result;
        }

        public static String RegexCheckApostrophePattern(String word)
        {
            String result = "";
            Regex regex = new Regex(patternApostrophe);
            MatchCollection matcheditem = regex.Matches(word);
            if (matcheditem.Count > 0)
                result = matcheditem[0].Value;
            return result;
        }

        public static List<String> GetSplitWords(String message)
        {
            String[] splitWords;
            splitWords = message.Split(' ');
            List<String> words = new List<String>();
            foreach (String word in splitWords)
                if (!word.Equals(""))
                    words.Add(word);
            return words;
        }

        private static String ConvertWord(String initialWord)
        {
            StringBuilder finalWord = new StringBuilder();

            for (int index = 1; index < initialWord.Length - 1; ++index)
                if(!initialWord[index].Equals(" "))
                finalWord.Append(initialWord[index]);

            return finalWord.ToString();
        }

        public static List<MessageItem> GetFormatMessage(String message)
        {
            List<String> words = GetSplitWords(message);

            List<MessageItem> formatWords = new List<MessageItem>();
            foreach(String word in words)
            {
                String underlineResult = RegexCheckUnderlinePattern(word);
                String starResult = RegexCheckStarPattern(word);
                String tildaResult = RegexCheckTildaPattern(word);
                String apostropheResult = RegexCheckApostrophePattern(word);

                if(!underlineResult.Equals(""))
                {
                    formatWords.Add(new MessageItem(ConvertWord(underlineResult), RegexType.UNDERLINE));
                }
                else
                if(!starResult.Equals(""))
                {
                    formatWords.Add(new MessageItem(ConvertWord(starResult), RegexType.STAR));
                }
                else
                if(!tildaResult.Equals(""))
                {
                    formatWords.Add(new MessageItem(ConvertWord(tildaResult), RegexType.TILDA));
                }
                else
                if(!apostropheResult.Equals(""))
                {
                    formatWords.Add(new MessageItem(ConvertWord(apostropheResult), RegexType.APOSTROPHE));
                }
                else
                {
                    formatWords.Add(new MessageItem(word, RegexType.NONE));
                }
            }
            return formatWords;
        }

    }
}
