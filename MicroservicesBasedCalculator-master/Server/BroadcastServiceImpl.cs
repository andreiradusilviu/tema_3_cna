﻿using Grpc.Core;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using grpc = global::Grpc.Core;

namespace Server
{
    public class BroadcastServiceImpl : Generated.Broadcast.BroadcastBase
    {

        private readonly ChatRoom _chatroomService;

        private List<IServerStreamWriter<Generated.Message>> serverStreams = new List<IServerStreamWriter<Generated.Message>>();
        public BroadcastServiceImpl(ChatRoom chatRoomService)
        {
            _chatroomService = chatRoomService;
        }

        public override async Task CreateStream(IAsyncStreamReader<Generated.Message> requestStream, IServerStreamWriter<Generated.Message> responseStream, ServerCallContext context)
        {
                if (!await requestStream.MoveNext()) return;

                do
                {
                    _chatroomService.Join(requestStream.Current.User.DisplayName, responseStream);
                    await _chatroomService.BroadcastMessageAsync(requestStream.Current);
                } while (await requestStream.MoveNext());

                _chatroomService.Remove(context.Peer);
        }


    }
}
