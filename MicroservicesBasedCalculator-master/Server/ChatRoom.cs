﻿
using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;

namespace Server
{
    public class ChatRoom
    {

        private ConcurrentDictionary<string, IServerStreamWriter<Generated.Message>> users = new ConcurrentDictionary<string, IServerStreamWriter<Generated.Message>>();

        public void Join(string name, IServerStreamWriter<Generated.Message> response) => users.TryAdd(name, response);

        public void Remove(string name) => users.TryRemove(name, out var s);

        public async Task BroadcastMessageAsync(Generated.Message message) => await BroadcastMessages(message);

        private async Task BroadcastMessages(Generated.Message message)
        {
            foreach (var user in users)
            {
                var item = await SendMessageToSubscriber(user, message);
                if (item != null)
                {
                    Remove(item?.Key);
                };
            }
        }

        private async Task<Nullable<KeyValuePair<string, IServerStreamWriter<Generated.Message>>>> SendMessageToSubscriber(KeyValuePair<string, IServerStreamWriter<Generated.Message>> user, Generated.Message message)
        {
            try
            {
                await user.Value.WriteAsync(message);
                return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return user;
            }
        }
    }
}

